.PHONY: all
.PHONY: clean

all: ./bin/main

IDIR := ./include

ODIR := ./.obj

SRCDIR := ./src

BINDIR := ./bin

.PHONY: $(BOARD_NAME)

$(BOARD_NAME):

EXE := $(BINDIR)/main

HDRS := \
    $(IDIR)/Spi_it.h

$(ODIR)/Spi_it.o : $(SRCDIR)/Spi_it.c $(HDRS)
	mkdir -p $(dir $@)
	$(CC) -I $(IDIR) -c -o $@ $<

$(ODIR)/main.o : $(SRCDIR)/main.c $(HDRS)
	mkdir -p $(dir $@)
	$(CC) -I $(IDIR) -c -o $@ $<

$(EXE) : $(ODIR)/Spi_it.o $(ODIR)/main.o
	mkdir -p $(BINDIR)
	$(CC) -o $@ $^ $(CFLAGS)

all : $(EXE)

clean:
	rm -rf $(ODIR)
	rm -rf $(BINDIR)
